﻿1
00:10:47,200 --> 00:10:47,633
Alright.

2
00:10:47,684 --> 00:10:52,964
Well, it should be clear to everyone now that you're welcome to Akademy.

3
00:10:53,620 --> 00:10:55,484
This is a very special Akademy.

4
00:10:57,130 --> 00:11:00,536
Everyone like me who has been
around for awhile,

5
00:11:00,537 --> 00:11:03,928
will see that, while it's still Akademy,

6
00:11:03,929 --> 00:11:08,238
we're at home and not like other years

7
00:11:08,273 --> 00:11:13,670
where actually we're all gathering and sharing our experiences together

8
00:11:14,730 --> 00:11:17,340
It's been a very special year though.

9
00:11:18,260 --> 00:11:20,390
I think that it won't escape anyone.

10
00:11:20,650 --> 00:11:22,520
We hope everyone is well and safe.

11
00:11:22,600 --> 00:11:25,810
We're doing it like this exactly for that.

12
00:11:26,620 --> 00:11:30,850
We can't meet but there's a lot of new opportunities that come from that.

13
00:11:31,730 --> 00:11:35,596
We get to not have to travel to somewhere

14
00:11:35,665 --> 00:11:38,137
to do the Akademy

15
00:11:38,144 --> 00:11:41,190
and I think that this is also a great opportunity, so

16
00:11:43,020 --> 00:11:48,477
You can share, we can share all of our Akademy contents and endeavors

17
00:11:48,552 --> 00:11:52,570
with everyone who wouldn't have been able to come anyway.

18
00:11:55,050 --> 00:11:58,109
We're gonna have talks today and tomorrow

19
00:11:58,471 --> 00:12:04,641
about a lot of stuff important to our community.

20
00:12:04,795 --> 00:12:10,066
I encourage everyone to take part of the conversations

21
00:12:10,090 --> 00:12:13,141
and think about it not just about talks,

22
00:12:13,183 --> 00:12:15,643
or just about somewhere to learn,

23
00:12:15,748 --> 00:12:18,596
but to engage and to start to

24
00:12:19,571 --> 00:12:22,742
work with the different people who make KDE

25
00:12:22,760 --> 00:12:24,699
who are very approachable

26
00:12:24,712 --> 00:12:27,579
and would like to work with you for sure.

27
00:12:29,700 --> 00:12:32,020
Remember as well that we will be having

28
00:12:32,080 --> 00:12:36,476
what we call BoFs, which are birds of feather sessions

29
00:12:36,798 --> 00:12:41,654
during the week where you will get to collaborate

30
00:12:41,731 --> 00:12:44,627
and start working together

31
00:12:44,980 --> 00:12:48,506
to discuss different important topics to the community

32
00:12:48,547 --> 00:12:52,768
and come to conclusions so that good work can happen

33
00:12:55,790 --> 00:12:56,850
And in the end,

34
00:12:57,320 --> 00:13:01,522
while this is a very new thing we're doing as KDE,

35
00:13:01,971 --> 00:13:05,292
virtual coordination has been in KDE's core since the beginning

36
00:13:05,327 --> 00:13:07,511
with being always an international community,

37
00:13:07,676 --> 00:13:12,810
and I think that it's great that we've been able

38
00:13:13,083 --> 00:13:14,122
to put this together

39
00:13:14,134 --> 00:13:19,070
especially with <?> the organizers who have done a great job.

40
00:13:19,252 --> 00:13:23,358
Now I don't really know how this works on virtual conferences,

41
00:13:23,401 --> 00:13:25,046
but give them an applause.

42
00:13:25,097 --> 00:13:28,512
I will hope that if you applaud at home,

43
00:13:28,517 --> 00:13:30,028
they will feel it somehow.

44
00:13:30,491 --> 00:13:30,971
Do it.

45
00:13:31,208 --> 00:13:35,522
If there is a way to do an applause virtually,

46
00:13:35,970 --> 00:13:36,577
do it.

47
00:13:36,813 --> 00:13:37,963
and it will be better.

48
00:13:38,109 --> 00:13:38,789
I don't really know.

49
00:13:39,100 --> 00:13:43,204
You can set yourself a status on BigBlueButton,

50
00:13:43,254 --> 00:13:45,667
maybe that's the way to applaud.

51
00:13:45,830 --> 00:13:48,460
But in the case, very good work from the organizers.

52
00:13:48,772 --> 00:13:50,714
They've been working tirelessly

53
00:13:52,123 --> 00:13:53,444
to make this happen.

54
00:13:55,150 --> 00:13:56,987
And thanks to the sponsors as well.

55
00:13:57,063 --> 00:14:00,282
They have been a crucial part

56
00:14:00,317 --> 00:14:02,685
for this Akademy to happen

57
00:14:05,440 --> 00:14:07,762
They are Canonical, KDAB, MBition,

58
00:14:07,824 --> 00:14:09,528
the openSUSE project, GitLab,

59
00:14:10,000 --> 00:14:12,896
Pine64, The Qt Company, Collabora,

60
00:14:13,643 --> 00:14:15,608
froglogic, Codethink,

61
00:14:21,823 --> 00:14:23,663
Dorota systems programmer

62
00:14:25,581 --> 00:14:26,982
Big thanks to all of them,

63
00:14:27,003 --> 00:14:29,604
without them Akademy would not be useful.

64
00:14:29,680 --> 00:14:32,157
Also, I think that it's very important to have them over

65
00:14:32,191 --> 00:14:35,896
and see what we do and work with us all together.

66
00:14:35,903 --> 00:14:38,868
This is what open source and free software is all about.

67
00:14:40,290 --> 00:14:44,319
I hope that this last year wasn't too harsh

68
00:14:44,381 --> 00:14:46,064
for you and your family.

69
00:14:46,085 --> 00:14:47,560
It's very hard times.

70
00:14:49,930 --> 00:14:54,401
Well, let's stay in, work, discuss,
and...

71
00:14:55,770 --> 00:14:59,284
Let's hope that we can meet again soon.

72
00:14:59,460 --> 00:15:02,478
Remember to clean your hands, and

73
00:15:03,180 --> 00:15:05,290
happy hacking!

74
00:15:05,510 --> 00:15:06,481
Now...

75
00:15:08,013 --> 00:15:10,174
I'm gonna pass on to Gina

76
00:15:10,175 --> 00:15:13,329
who will tell us about the adventures

77
00:15:13,358 --> 00:15:15,156
of the open source development.

78
00:15:16,770 --> 00:15:18,024
Looking forward to her.

