﻿1
00:00:12,589 --> 00:00:16,135
I hope everyone can see me well,
and I hope everyone can hear me well.

2
00:00:19,429 --> 00:00:22,433
A small thumbs up in the chat
would be nice if you can hear me well

3
00:00:22,466 --> 00:00:24,808
because this is also the first time
that I'm using this.

4
00:00:29,223 --> 00:00:30,871
Then let's get this started, I guess.

5
00:00:30,971 --> 00:00:33,597
First of all, it's a huge honor to be here,

6
00:00:33,614 --> 00:00:35,778
I really want to thank everyone.

7
00:00:36,237 --> 00:00:39,866
This is the first keynote I'm ever
giving in my life, so far.

8
00:00:40,022 --> 00:00:43,068
I'm very excited, I hope it
doesn't show too much.

9
00:00:45,206 --> 00:00:47,990
First of all, who am I in the first place.

10
00:00:48,094 --> 00:00:51,020
You already know my name,
it's Gina Häußge.

11
00:00:51,243 --> 00:00:55,130
I'm a 37 year old full-time nerd,
so to speak,

12
00:00:56,242 --> 00:00:58,941
software engineer by trade and by heart,

13
00:00:59,261 --> 00:01:00,746
maker, hobby baker,

14
00:01:00,801 --> 00:01:03,415
and I have to add that the
hobby baker thing is not a thing

15
00:01:03,424 --> 00:01:04,887
that started with the pandemic,

16
00:01:04,925 --> 00:01:08,185
but it's something that I actually
did before as well.

17
00:01:08,550 --> 00:01:11,978
And I'm also the creator
and maintainer of OctoPrint.

18
00:01:13,250 --> 00:01:16,255
I would now, before
continuing to talk about that,

19
00:01:16,339 --> 00:01:22,330
I would like to know who of you
has heard or used OctoPrint,

20
00:01:23,286 --> 00:01:26,326
has heard or used OctoPrint so far.

21
00:01:30,924 --> 00:01:32,566
So I'm going to give this...

22
00:01:33,984 --> 00:01:34,962
...some quick time.

23
00:01:34,989 --> 00:01:38,558
But yeah, it looks like the majority
has not heard or used it so far.

24
00:01:38,844 --> 00:01:40,631
Heard about or used it so far.

25
00:01:40,691 --> 00:01:42,316
English in the morning is tricky.

26
00:01:50,193 --> 00:01:53,738
Then let's maybe explain first what
OctoPrint is in the first place

27
00:01:53,758 --> 00:01:56,554
so that you know from what
direction I'm talking today.

28
00:01:56,928 --> 00:01:59,884
OctoPrint is a web interface
for consumer 3D printers,

29
00:02:00,247 --> 00:02:03,496
those things that these days you
actually can buy in the hardware stores.

30
00:02:04,347 --> 00:02:06,429
It's an open-source project,

31
00:02:06,618 --> 00:02:08,712
licensed under the AGPLv3.

32
00:02:09,713 --> 00:02:14,386
Currently it has something around
100,000 confirmed users worldwide.

33
00:02:14,429 --> 00:02:18,363
I don't know an actual number
because the user tracking that I do

34
00:02:18,380 --> 00:02:21,211
is obviously opt-in due to privacy reasons,

35
00:02:21,487 --> 00:02:24,933
so I don't know about you
if you're using it or not

36
00:02:24,983 --> 00:02:27,181
unless you tell me about it.

37
00:02:27,452 --> 00:02:29,501
It's written in Python for the backend,

38
00:02:29,534 --> 00:02:32,580
and HTML and CSS and
JavaScript for the front end,

39
00:02:32,615 --> 00:02:35,524
and you can find all
about it on octoprint.org.

40
00:02:35,883 --> 00:02:41,596
I wrote this thing out of an
own personal itch to scratch,

41
00:02:41,616 --> 00:02:46,105
because back in 2012, I got myself
my first owned 3D printer,

42
00:02:46,190 --> 00:02:49,937
and found myself in the situation that
it was now taking up space in my office,

43
00:02:49,966 --> 00:02:52,148
and producing fumes and noises and all that,

44
00:02:52,150 --> 00:02:56,633
and that was not that great to sit
next to while hacking on stuff.

45
00:02:56,703 --> 00:03:00,034
So I wanted a way to control it remotely,

46
00:03:00,070 --> 00:03:03,193
and this is when I sat down
over my Christmas break

47
00:03:03,543 --> 00:03:06,488
and started working on OctoPrint.

48
00:03:07,578 --> 00:03:11,280
We are now looking at almost
8 years of history of this project.

49
00:03:13,388 --> 00:03:19,685
The popularity was actually
big enough that back in 2014,

50
00:03:19,711 --> 00:03:20,929
so 6 years ago already,

51
00:03:20,958 --> 00:03:23,130
I was able to go full time with OctoPrint.

52
00:03:23,364 --> 00:03:25,504
Back then I got hired by a company,

53
00:03:25,626 --> 00:03:28,701
a 3D printer company who employed me

54
00:03:28,743 --> 00:03:32,052
to work full time on
moving OctoPrint forward.

55
00:03:32,479 --> 00:03:35,663
Then in April 2016, I had to
switch to crowdfunding

56
00:03:35,683 --> 00:03:37,197
because they ran out of money.

57
00:03:40,238 --> 00:03:43,459
It has been a wild ride so to speak,

58
00:03:43,502 --> 00:03:48,170
and it has been quite the adventure
over the course of all these 8 years.

59
00:03:48,218 --> 00:03:54,462
You see there have been a lot of releases,
which are these little green markers there.

60
00:03:54,756 --> 00:03:58,696
There has been a lot of
improvement around...

61
00:04:02,334 --> 00:04:04,947
...the infrastructure and all that.

62
00:04:06,202 --> 00:04:08,918
It has been really an exciting time.

63
00:04:09,804 --> 00:04:14,295
I learned a lot about things
in the meantime.

64
00:04:15,358 --> 00:04:19,198
I want to talk today a bit about

65
00:04:20,143 --> 00:04:21,989
about the experiences that I had,

66
00:04:22,005 --> 00:04:24,573
or rather the lessons
that I took from them.

67
00:04:24,870 --> 00:04:30,876
So, what it is like to run an open source
project for 8 years of your life,

68
00:04:31,045 --> 00:04:32,577
6 of those full-time.

69
00:04:35,068 --> 00:04:40,411
Also, how it is to run an end-user facing
piece of software for so long

70
00:04:40,438 --> 00:04:46,964
because OctoPrint is more targeting
your run-off-the-mill 3D printer user,

71
00:04:47,179 --> 00:04:49,190
and those are not necessarily developers.

72
00:04:49,212 --> 00:04:51,300
And I'll also talk about that a bit later,

73
00:04:51,338 --> 00:04:52,688
and what that means for...

74
00:04:52,811 --> 00:04:55,018
what implications that has.

75
00:04:57,957 --> 00:05:03,159
I'm going to talk about the good,
the bad and the ugly that I experienced.

76
00:05:03,235 --> 00:05:05,974
So let's dive right in, I guess,

77
00:05:06,052 --> 00:05:07,715
and start with the good.

78
00:05:11,310 --> 00:05:15,471
Obviously when you're working
on your own software project this long,

79
00:05:15,502 --> 00:05:18,171
on your own open source
project for this long,

80
00:05:18,234 --> 00:05:19,482
it is your project.

81
00:05:19,624 --> 00:05:22,250
What does this mean?
Well, it means you can shape it.

82
00:05:22,272 --> 00:05:24,514
It's your vision that you implement,

83
00:05:24,578 --> 00:05:28,293
you do not have to trust anyone else
to make decisions for you,

84
00:05:28,309 --> 00:05:31,187
you do not have to give away
control and all that,

85
00:05:31,491 --> 00:05:34,453
and you do not have to face
what I would like to call

86
00:05:34,481 --> 00:05:37,548
politics-driven architecture,
which is something that I experienced

87
00:05:37,571 --> 00:05:40,457
within my former life as a
corporate software engineer.

88
00:05:40,699 --> 00:05:44,798
where technical and architecture
decisions are made

89
00:05:44,826 --> 00:05:47,953
based on internal department
politics sometimes,

90
00:05:48,121 --> 00:05:50,187
and not on what is the best choice

91
00:05:50,224 --> 00:05:54,063
for this particular problem
that we are facing here.

92
00:05:54,679 --> 00:05:56,591
And also very important obviously,

93
00:05:56,612 --> 00:06:00,971
is you work on something
that you actually use yourself,

94
00:06:01,082 --> 00:06:03,284
that you enjoy using yourself.

95
00:06:03,648 --> 00:06:06,975
I have... right next to me here
I have two printers

96
00:06:07,016 --> 00:06:11,075
that would be up and running
if I wasn't giving this talk right now.

97
00:06:11,499 --> 00:06:14,949
I also have a laser cutter back there
that runs OctoPrint as well.

98
00:06:15,827 --> 00:06:19,786
So I use my own software daily,
or more or less daily,

99
00:06:19,813 --> 00:06:22,129
and this is a really really great feeling.

100
00:06:23,228 --> 00:06:27,275
This is something that makes
working on an open source project,

101
00:06:27,302 --> 00:06:30,002
even for that long, very very enjoyable.

102
00:06:30,198 --> 00:06:33,022
Another thing that makes it
very very enjoyable,

103
00:06:33,043 --> 00:06:35,655
is the fact that you help people.

104
00:06:35,897 --> 00:06:38,522
So this is also something
that I really really enjoy,

105
00:06:38,878 --> 00:06:43,494
and that I have come to cherish
as a really important part

106
00:06:43,522 --> 00:06:45,476
of my day to-day life.

107
00:06:48,444 --> 00:06:52,920
People have problems and you help them.

108
00:06:52,921 --> 00:06:56,503
You help them solve their problems.
You enable them to overcome their problems,

109
00:06:56,840 --> 00:06:58,444
by providing them with a solution.

110
00:06:58,479 --> 00:07:01,368
And sometimes they even say thank you.

111
00:07:02,357 --> 00:07:05,144
I get the occassional email here and there,

112
00:07:05,173 --> 00:07:07,624
or just the tweet in the morning
or something like that,

113
00:07:07,649 --> 00:07:09,268
that just says "thank you".

114
00:07:09,291 --> 00:07:14,853
And that makes all of the bitter arguing
that we will be talking about later

115
00:07:14,898 --> 00:07:16,195
so much worth it.

116
00:07:17,191 --> 00:07:20,504
I have gotten a lot of feedback
over the years,

117
00:07:20,521 --> 00:07:23,899
and one of the most funny ones
that I ever got was

118
00:07:24,793 --> 00:07:28,171
a guy who messaged me and
told me that I saved his marriage,

119
00:07:28,204 --> 00:07:34,187
because he was spending so much time
with his printer in his garage all the time,

120
00:07:34,198 --> 00:07:36,439
because he had to babysit it basically,

121
00:07:36,514 --> 00:07:38,078
and the wife and the kids were not amused.

122
00:07:38,147 --> 00:07:41,085
Then he installed OctoPrint,
and then he could get back

123
00:07:41,187 --> 00:07:44,841
into spending time with them,
and just sneakily keeping an eye

124
00:07:44,863 --> 00:07:47,877
on his print while doing that.

125
00:07:49,893 --> 00:07:52,618
That's not something you hear every day,

126
00:07:52,646 --> 00:07:56,193
especially not in non-
open source work I think.

127
00:08:01,264 --> 00:08:03,857
Another thing that is very very
dear to my heart

128
00:08:03,897 --> 00:08:07,580
and a huge advantage of working
on an open source project for this long,

129
00:08:07,634 --> 00:08:09,854
or even just working on
open source project in general,

130
00:08:10,107 --> 00:08:12,579
is you learn tons of new stuff.

131
00:08:12,961 --> 00:08:15,276
I'm not just talking about
software development itself.

132
00:08:15,290 --> 00:08:19,787
Obviously my coding skills have increased
ever since I started working on OctoPrint.

133
00:08:19,830 --> 00:08:21,746
I constantly learn new libraries,

134
00:08:21,775 --> 00:08:23,896
I constantly learn new tricks up my sleeve,

135
00:08:23,952 --> 00:08:29,230
I learn more Python,
I get more proficient in this stuff.

136
00:08:29,859 --> 00:08:32,698
But when you run a project like this,

137
00:08:32,754 --> 00:08:34,934
you also need to learn about
project management.

138
00:08:35,555 --> 00:08:37,596
You need to learn about
community management,

139
00:08:37,618 --> 00:08:39,919
and how to manage your
community's expectations,

140
00:08:39,967 --> 00:08:44,101
how to give them a forum,
a platform to exchange things,

141
00:08:44,131 --> 00:08:49,720
exchange ideas, exchange solutions,
help each other, stuff like this.

142
00:08:49,797 --> 00:08:51,788
You have to learn about
release management.

143
00:08:52,600 --> 00:08:55,669
It's not just that you throw code
in a Git repository

144
00:08:55,694 --> 00:08:58,834
and people then pull it in and everything
is fine and dandy and works.

145
00:08:58,879 --> 00:09:03,714
But at some point you will realize
that at some growth of your project

146
00:09:03,780 --> 00:09:07,874
you really need to put out
stable, reliable builds

147
00:09:07,947 --> 00:09:10,554
that you can reproduce
and stuff like this.

148
00:09:14,147 --> 00:09:17,997
That is all that comes with running
such an open source project.

149
00:09:18,584 --> 00:09:20,224
Talking about release management,

150
00:09:20,229 --> 00:09:22,346
you also learn the value
of release candidates

151
00:09:22,378 --> 00:09:25,002
when you suddenly find yourself
on a Saturday morning

152
00:09:25,046 --> 00:09:26,773
trying to frantically fix a bug

153
00:09:26,863 --> 00:09:29,457
in the stable release
you pushed out on Friday.

154
00:09:30,988 --> 00:09:34,174
And you also learn how
bloody tricky it can be

155
00:09:34,197 --> 00:09:36,572
to get people to actually
run release candidates,

156
00:09:36,721 --> 00:09:39,000
which comes back to
community management

157
00:09:39,027 --> 00:09:43,834
where you need to try to incentivize
people to help you run the project,

158
00:09:44,005 --> 00:09:46,158
because you can't do
everything on your own.

159
00:09:47,445 --> 00:09:50,972
In case of OctoPrint, there was also
a bit of brand development involved.

160
00:09:51,041 --> 00:09:53,485
I needed a funny little logo,

161
00:09:53,546 --> 00:09:56,317
I needed some corporate identity,

162
00:09:56,334 --> 00:10:00,430
and all that, so that things
get recognizable.

163
00:10:02,024 --> 00:10:06,741
You learn not only about software
development when you run such a project.

164
00:10:06,786 --> 00:10:09,653
And I have to say
that I really enjoy that.

165
00:10:10,411 --> 00:10:14,363
If you like learning, running
an open source project is like

166
00:10:14,808 --> 00:10:18,269
a full overdose of learning.

167
00:10:19,813 --> 00:10:24,620
But as I already hinted,
it's not all fine and dandy, sadly.

168
00:10:25,167 --> 00:10:30,489
There are also some bad things that
you will have to face when doing this.

169
00:10:32,337 --> 00:10:36,734
One obvious big point,
probably for most of you,

170
00:10:36,777 --> 00:10:39,355
is the work-life balance situation.

171
00:10:39,632 --> 00:10:43,568
Especially if you run a project like
OctoPrint, or even something smaller,

172
00:10:44,469 --> 00:10:47,544
as a side project,
next to your own full time job,

173
00:10:47,692 --> 00:10:50,583
it can become a bit much.

174
00:10:50,687 --> 00:10:53,917
It can become really hard
to shut off and recharge.

175
00:10:54,949 --> 00:10:58,580
And you really need to learn to be
very protective of your private time.

176
00:10:58,898 --> 00:11:03,833
For example with OctoPrint,
you saw on the timeline

177
00:11:04,387 --> 00:11:08,533
the first two years almost,
I did it next to my own full time job.

178
00:11:08,565 --> 00:11:11,891
So after 40 hours per week
of regular work,

179
00:11:12,235 --> 00:11:17,198
I then also tried to shoulder everything
that I had to do with OctoPrint

180
00:11:17,246 --> 00:11:21,514
during my after-hours, right after work,
on the weekends, during vacation.

181
00:11:21,747 --> 00:11:24,437
And not only was that something
that friends and family

182
00:11:24,453 --> 00:11:26,486
did not find particularly funny after a while,

183
00:11:26,509 --> 00:11:33,336
but also noticed a negative impact on my
health and on my motivation as well.

184
00:11:33,607 --> 00:11:36,274
So this is something that you
need to always keep an eye out,

185
00:11:36,296 --> 00:11:39,388
in my personal experience,
because noone else will.

186
00:11:41,505 --> 00:11:43,147
You need to take breaks,

187
00:11:43,196 --> 00:11:46,199
you need to make sure that you
do not spend all your time,

188
00:11:46,255 --> 00:11:52,104
all your waking time in front of the PC
and code away on your pet project,

189
00:11:52,136 --> 00:11:54,114
or on your full-time project.

190
00:11:55,446 --> 00:11:57,622
It will break you long term.

191
00:11:58,559 --> 00:11:59,801
You can do that for a while,

192
00:11:59,823 --> 00:12:02,828
but you should have a plan B
in order to get away from this mode.

193
00:12:06,445 --> 00:12:09,775
Another bad thing, sadly, is pay.

194
00:12:09,926 --> 00:12:11,808
Noone likes to talk about money,

195
00:12:11,872 --> 00:12:15,265
but we like having it in order to
be able to pay our bills.

196
00:12:18,442 --> 00:12:23,601
Asking for donations on an open source
project is still a bit tough.

197
00:12:23,796 --> 00:12:25,531
You always feel a bit weird

198
00:12:25,568 --> 00:12:27,975
"here's something for free,
but also please give me money"

199
00:12:27,988 --> 00:12:32,367
don't ask me what kind of discussion
that was with my tax consultant.

200
00:12:34,078 --> 00:12:36,006
And it also usually doesn't scale.

201
00:12:36,329 --> 00:12:40,679
Usually what you get when you ask
for donations on an open source project

202
00:12:40,967 --> 00:12:42,743
will not be enough to pay the rent.

203
00:12:43,377 --> 00:12:47,005
It's more like beer money,
as some people would call it.

204
00:12:49,202 --> 00:12:53,839
Thankfully, I found that with OctoPrint...

205
00:12:53,880 --> 00:12:57,227
I faced this situation back in 2016,

206
00:12:57,433 --> 00:13:01,723
that I had to go donation-based
and crowdfunded full-time.

207
00:13:01,768 --> 00:13:06,712
And I realized that if you
really make it transparent,

208
00:13:06,740 --> 00:13:09,092
how much work an
open source project can be,

209
00:13:09,108 --> 00:13:11,130
and how much work
you're actually putting in,

210
00:13:11,258 --> 00:13:13,915
how much of yourself
you're pouring in, so to speak.

211
00:13:14,201 --> 00:13:19,820
And if you have a somewhat stable
user base, then it can actually work.

212
00:13:19,844 --> 00:13:23,459
In my case, so far, it is working.

213
00:13:26,713 --> 00:13:29,304
It's definitely working better targeting ─

214
00:13:29,385 --> 00:13:32,416
I feel a bit bad using this word but ─

215
00:13:32,888 --> 00:13:37,260
targeting end users rather than
companies, in my experience, weirdly.

216
00:13:37,302 --> 00:13:40,902
Because companies always have
this capitalistic approach of

217
00:13:40,918 --> 00:13:42,719
"what do I get back in return?", and

218
00:13:43,085 --> 00:13:46,934
"the software that you're using gets
maintained for the foreseeable future"

219
00:13:46,949 --> 00:13:51,313
apparently doesn't seem to suffice for
them as a reason to give you money.

220
00:13:56,473 --> 00:14:00,055
In order to be able to make this
a viable approach financially as well,

221
00:14:00,070 --> 00:14:03,103
to work on an open source project,
from my personal experience,

222
00:14:03,167 --> 00:14:06,246
and I do not say that
this is a general advice

223
00:14:06,322 --> 00:14:09,034
that is applicable to
every single project out there

224
00:14:09,058 --> 00:14:11,549
but it worked for OctoPrint
and its target audience,

225
00:14:11,804 --> 00:14:13,878
is you really should
make it easy for people

226
00:14:13,912 --> 00:14:15,903
to give you tiny but recurring tips.

227
00:14:16,101 --> 00:14:18,237
So, do not ask for "please give me

228
00:14:18,255 --> 00:14:21,605
"a one-time payment of $20
or $50 or $100" or something,

229
00:14:21,626 --> 00:14:24,733
but rather make it possible for them
to give you one dollar per month,

230
00:14:24,788 --> 00:14:26,030
or something like this.

231
00:14:26,414 --> 00:14:30,893
Because if you get 2000 or 3000 people
doing that, it quickly adds up.

232
00:14:32,983 --> 00:14:38,118
In order to do that, you should also
offer them various ways to do so.

233
00:14:38,957 --> 00:14:41,478
These days we have GitHub Sponsors,
there's Patreon,

234
00:14:41,519 --> 00:14:46,067
which is still what OctoPrint
primarily gets funded with.

235
00:14:46,265 --> 00:14:50,642
There is LiberaPay,
there is DonorBox,

236
00:14:50,833 --> 00:14:54,100
There is PayPal obviously
for one-time payments.

237
00:14:54,264 --> 00:14:56,817
At least in Germany you can
only do one-time payments

238
00:14:56,880 --> 00:14:58,557
unless you have a business account.

239
00:14:59,416 --> 00:15:03,368
Let's not dive into
these details, but still.

240
00:15:03,703 --> 00:15:04,848
It's a bit...

241
00:15:05,315 --> 00:15:07,246
You should give them choice,

242
00:15:07,509 --> 00:15:11,934
because if people do not like Patreon
and you only allow Patreon,

243
00:15:13,158 --> 00:15:16,220
then you won't get this donation.

244
00:15:17,016 --> 00:15:20,160
That has been my experience
in that regard.

245
00:15:21,839 --> 00:15:25,135
The final bad thing, solitude.

246
00:15:26,092 --> 00:15:29,244
Especially at the start of your project,
you will be doing it alone.

247
00:15:29,274 --> 00:15:33,773
And in the case of OctoPrint,
funnily enough I still mostly do it alone.

248
00:15:34,257 --> 00:15:37,703
The thing is that people are
really fast to request features,

249
00:15:37,711 --> 00:15:39,194
but slow to contribute them.

250
00:15:40,578 --> 00:15:44,453
It's easy to say "hey it would be great
if OctoPrint also did this and that",

251
00:15:44,483 --> 00:15:48,107
but actually making it do that,
and not only making it do that once,

252
00:15:48,188 --> 00:15:51,445
but then keeping that functionality

253
00:15:51,479 --> 00:15:54,556
maintained through the years, that is hard.

254
00:15:54,668 --> 00:15:59,179
Or at least it is trickier or more work
than just saying "hey it would be nice".

255
00:16:04,111 --> 00:16:06,582
The thing is...

256
00:16:09,461 --> 00:16:12,758
From my personal experiences, you should
make it easy for people to help you,

257
00:16:12,784 --> 00:16:16,195
but without having to then
maintain whatever they added.

258
00:16:16,416 --> 00:16:20,666
So something like a plugin system
can go a long long way here.

259
00:16:22,244 --> 00:16:25,326
Alright, and now, while we're
talking about the bad things,

260
00:16:25,336 --> 00:16:26,903
let's go to the ugly things.

261
00:16:29,069 --> 00:16:35,236
There are sadly some...
entitlement and attacks.

262
00:16:35,390 --> 00:16:39,838
This is something that I fear a lot of you
have seen in the open source space.

263
00:16:39,878 --> 00:16:42,820
You have users that want
something immediately done,

264
00:16:42,886 --> 00:16:47,504
and who treat you a bit like their
personal slave or something like this,

265
00:16:48,080 --> 00:16:52,046
People shaming you for pushing
a release because it contained a bug.

266
00:16:52,095 --> 00:16:55,294
And yes, these are all things
that I've actually heard.

267
00:16:57,057 --> 00:16:59,182
People who outright insult you.

268
00:16:59,195 --> 00:17:07,404
And I do not want to collect examples
of all of this here, but yeah...

269
00:17:08,965 --> 00:17:12,033
The problem is people like this
can really ruin your day

270
00:17:12,041 --> 00:17:13,575
as an open source developer.

271
00:17:13,771 --> 00:17:17,288
And the important thing to remember
here at all times is really

272
00:17:18,130 --> 00:17:21,666
Yes, those are vocal, and
they can ruin your day.

273
00:17:21,721 --> 00:17:28,084
But the silent majority who is happy
with using what you put out,

274
00:17:28,124 --> 00:17:32,173
and who is grateful for
the work you do, is there.

275
00:17:32,420 --> 00:17:36,760
And they may be silent,
but they are definitely the majority.

276
00:17:37,085 --> 00:17:39,431
And this is just something
that you have to keep in mind.

277
00:17:40,373 --> 00:17:43,535
What you also have to
keep in mind is, in my opinion,

278
00:17:43,562 --> 00:17:49,164
a lot of people will tell you
"this is just like it is in open source"

279
00:17:49,223 --> 00:17:52,134
and "you just have to grow
a thick skin as a maintainer"

280
00:17:52,162 --> 00:17:54,273
and I do not agree with this,
I have to say.

281
00:17:54,297 --> 00:17:57,194
I think we should just--
as maintainers, we should say

282
00:17:57,221 --> 00:18:00,969
"no, this is a boundary that
I'm not allowing you to cross".

283
00:18:08,199 --> 00:18:11,406
The other thing that you might
face is license violations.

284
00:18:11,506 --> 00:18:13,459
Some company stealing your code,

285
00:18:18,028 --> 00:18:21,542
Or you are just thinking that
maybe some company stole your code,

286
00:18:24,271 --> 00:18:26,912
This is something that feels
really really bad.

287
00:18:26,945 --> 00:18:29,066
I have not had definitely happen that.

288
00:18:29,943 --> 00:18:32,237
Sorry, my English is just
confused right now.

289
00:18:32,272 --> 00:18:35,124
I haven't definitely had that
happen with OctoPrint so far,

290
00:18:35,137 --> 00:18:37,554
but I have had some cases
where I suspected

291
00:18:37,590 --> 00:18:42,147
that someone was using my software
internally in their product

292
00:18:42,181 --> 00:18:45,489
without actually adhering to the license.

293
00:18:46,924 --> 00:18:50,253
The thing is, I could have
tried to figure that out,

294
00:18:50,267 --> 00:18:54,131
I could have tried to drag them
into a legal battle over this,

295
00:18:54,221 --> 00:18:56,078
got myself a lawyer, paid the lawyer

296
00:18:56,118 --> 00:18:59,070
with the limited amounts of funds
that I get and all that.

297
00:19:00,235 --> 00:19:02,994
Try to publicly shame them,
something like this,

298
00:19:03,036 --> 00:19:06,597
but if it's just a sneaky suspicion,
from my experience,

299
00:19:07,767 --> 00:19:10,037
I'm not sure if it's always worth it.

300
00:19:10,072 --> 00:19:13,595
So what I'm trying to say here is,
choose your battles wisely.

301
00:19:13,777 --> 00:19:15,360
License violations suck.

302
00:19:15,916 --> 00:19:19,210
License violations are something
that are really

303
00:19:19,457 --> 00:19:21,845
fully against the nature of open source,

304
00:19:22,048 --> 00:19:24,577
but on the other hand,
if you're a maintainer,

305
00:19:24,885 --> 00:19:27,652
if you're a lone maintainer especially,

306
00:19:27,857 --> 00:19:31,722
then you cannot fight
all the battles out there.

307
00:19:31,760 --> 00:19:34,942
So if you have the possibility
to outsource this

308
00:19:34,997 --> 00:19:38,118
to something like gplviolations.org
or something, I don't know,

309
00:19:38,334 --> 00:19:43,677
then maybe do that, but try not to get
worked up too much about this as well,

310
00:19:43,842 --> 00:19:46,498
from my personal experience
usually it's simply not worth it,

311
00:19:46,522 --> 00:19:49,505
because your product is
still the popular one.

312
00:19:53,358 --> 00:19:55,572
And the final ugly side of things,

313
00:19:55,653 --> 00:19:59,200
and something that I guess
all of you have also faced

314
00:19:59,486 --> 00:20:01,475
one time or another during your life,

315
00:20:01,517 --> 00:20:03,670
is the risk of burnout.

316
00:20:04,814 --> 00:20:07,844
We've seen that there's a lot of stuff
that you constantly have to learn.

317
00:20:08,008 --> 00:20:11,334
There is quite a number of things
that you have to worry about.

318
00:20:11,505 --> 00:20:14,269
Payment, work-life balance,
and all of that.

319
00:20:14,318 --> 00:20:17,118
And on top of this, there are
also the ugly side of things,

320
00:20:17,514 --> 00:20:21,697
entitled users, personal attacks,
insults, that you have to face.

321
00:20:22,409 --> 00:20:25,809
GPL violations, and stuff like this.

322
00:20:25,865 --> 00:20:30,437
And that can really really
weigh down on you.

323
00:20:31,893 --> 00:20:35,234
So burnout for open source
maintainers is a real threat

324
00:20:35,288 --> 00:20:40,450
and I would be lying if I said
I have not faced it in the past.

325
00:20:40,868 --> 00:20:43,640
And what I would really
recommend here is

326
00:20:43,812 --> 00:20:48,378
if you do regular open source work,
or just if you work in general,

327
00:20:48,464 --> 00:20:51,173
but especially if you do
regular open source work,

328
00:20:51,342 --> 00:20:54,982
really keep a close eye on
your own mental health

329
00:20:55,059 --> 00:20:56,784
and your own physical health.

330
00:20:57,767 --> 00:21:00,234
Learn to spot warning signs of burning out

331
00:21:00,244 --> 00:21:03,927
like increased apathy or irritability.

332
00:21:04,338 --> 00:21:10,255
In general, not feeling all there or
feeling like everything sucks and all that,

333
00:21:10,297 --> 00:21:16,051
because this can spiral out of control
quickly and you do not want that.

334
00:21:16,080 --> 00:21:18,157
You do not want to burn out
on your open source project

335
00:21:18,203 --> 00:21:20,520
because, as we saw,
it is a work of passion.

336
00:21:20,527 --> 00:21:22,195
It is something where you help people.

337
00:21:22,407 --> 00:21:23,645
It is something where you learn so much.

338
00:21:23,653 --> 00:21:27,311
So it would be a real pity
if due to other factors

339
00:21:27,353 --> 00:21:29,950
you burn out on it
and cannot do it anymore.

340
00:21:30,281 --> 00:21:33,724
And for that it's really
important to know that

341
00:21:34,562 --> 00:21:38,339
you do not owe anyone anything
when you work on open source.

342
00:21:38,584 --> 00:21:40,217
You give away your work for free,

343
00:21:40,478 --> 00:21:42,632
you're helping people for free,

344
00:21:43,866 --> 00:21:46,877
and this is already what
they are getting from you.

345
00:21:46,924 --> 00:21:49,408
They cannot demand from you
to never take vacation days,

346
00:21:49,444 --> 00:21:51,286
or never have a weekend
or something like that,

347
00:21:51,328 --> 00:21:53,441
so you should not either.

348
00:21:54,389 --> 00:21:56,131
You need to take care of yourself,

349
00:21:56,159 --> 00:21:57,911
because noone else will.

350
00:21:58,628 --> 00:22:01,796
So what's really really
important here is

351
00:22:01,935 --> 00:22:05,998
you need to learn to say no
as an open source maintainer.

352
00:22:06,016 --> 00:22:08,402
"No" to this feature,
"no" to this bug report,

353
00:22:08,431 --> 00:22:10,569
and also "no" to this
discussion yet again

354
00:22:10,595 --> 00:22:14,181
about a topic that you've
talked about endlessly,

355
00:22:14,209 --> 00:22:16,743
or just something that is
currently not something

356
00:22:16,762 --> 00:22:19,719
that you have enough spoons to tackle.

357
00:22:23,166 --> 00:22:25,150
These are the ugly sides.

358
00:22:25,622 --> 00:22:30,986
The question of course is with all that
negativity that we just talked about,

359
00:22:31,280 --> 00:22:32,503
would I do it again?

360
00:22:32,521 --> 00:22:37,463
Would I do all this journey,
would I go on this adventure again?

361
00:22:37,473 --> 00:22:41,604
And the answer is an
absolutely resounding "yes".

362
00:22:42,840 --> 00:22:47,053
It is a very very wild ride.

363
00:22:47,142 --> 00:22:51,621
I have learned so much,
I have so many good memories

364
00:22:51,718 --> 00:22:55,151
thanks to working on OctoPrint,
and I'm still making new ones.

365
00:22:56,718 --> 00:23:00,126
It is something that is very humbling,

366
00:23:00,146 --> 00:23:03,311
but at the same time also
something that allows you

367
00:23:03,333 --> 00:23:06,798
for so much personal growth
and also professional growth.

368
00:23:07,179 --> 00:23:11,097
And it is really rewarding as well.

369
00:23:11,118 --> 00:23:14,926
Especially when you get these tiny
mails or tweets here and there

370
00:23:15,040 --> 00:23:16,768
that say "thank you".

371
00:23:18,320 --> 00:23:21,127
I would absolutely do all of this again.

372
00:23:21,342 --> 00:23:23,982
I would probably change
some things here and there,

373
00:23:24,037 --> 00:23:26,662
because I learned a lot,
which I would put to good use.

374
00:23:26,798 --> 00:23:29,257
But I would totally do it again.

375
00:23:31,096 --> 00:23:34,890
And with that, I just want to
thank you for your attention.

376
00:23:37,039 --> 00:23:38,585
I hope it was interesting.

377
00:23:38,675 --> 00:23:42,728
If you have any questions that
I will not be able to cover here anymore,

378
00:23:43,132 --> 00:23:45,644
you can reach me on Twitter @foosel.

379
00:23:46,101 --> 00:23:49,959
I will also put the slides up
on octoprint.org/slides later on.

380
00:23:49,991 --> 00:23:52,787
And with that, I just want to say thank you.

