﻿1
00:00:11,307 --> 00:00:16,245
One year ago, the Consistency goal was
selected during the 2019 Akademy in Milano.

2
00:00:16,527 --> 00:00:19,334
A very scared me was invited
on the stage to better introduce it.

3
00:00:19,403 --> 00:00:21,998
Now I'm here again to talk
about how is it going

4
00:00:22,014 --> 00:00:25,375
after one year of developing with KDE.

5
00:00:25,531 --> 00:00:27,067
To be honest, I'm still a bit scared,

6
00:00:27,081 --> 00:00:30,702
but at least I don't have to go on stage,

7
00:00:30,737 --> 00:00:36,654
because last year I was a bit
embarrassed as you can see here.

8
00:00:38,337 --> 00:00:41,814
Let's start immediately with
what is consistency anyway.

9
00:00:41,841 --> 00:00:46,337
So, consistency here means having
a single solution for a particular problem

10
00:00:46,366 --> 00:00:47,992
rather than many different ones.

11
00:00:48,135 --> 00:00:51,195
This applies to application design elements,
feature implementations,

12
00:00:51,216 --> 00:00:53,324
website structure and style,

13
00:00:53,407 --> 00:00:58,783
and the KDE ecosystem as a whole that,
unfortunately, often suffers from redundancy.

14
00:00:58,943 --> 00:01:04,954
Now, even though I wrote the above,
it still feels a bit like jargon,

15
00:01:04,989 --> 00:01:08,900
so let's see an actual example of consistency.

16
00:01:08,975 --> 00:01:10,702
The search bar.

17
00:01:10,872 --> 00:01:14,674
The search bar is a trivial task,

18
00:01:14,947 --> 00:01:19,221
We have different applications that say

19
00:01:19,399 --> 00:01:21,188
"I need a search bar, that sounds easy,

20
00:01:21,190 --> 00:01:23,014
I'll just do my own implementation,

21
00:01:23,044 --> 00:01:24,422
what could go wrong?"

22
00:01:24,794 --> 00:01:27,150
And it does go wrong.

23
00:01:28,836 --> 00:01:31,679
We can see here that we have disabled buttons,

24
00:01:31,721 --> 00:01:33,161
and here they are active.

25
00:01:33,251 --> 00:01:36,258
We have no arrow here,
and we have an arrow here.

26
00:01:36,313 --> 00:01:38,596
We have arrow here, no arrow here.

27
00:01:38,760 --> 00:01:40,479
We have the "Find" as text here,

28
00:01:40,569 --> 00:01:44,263
and then it becomes inside the text box.

29
00:01:44,475 --> 00:01:46,796
Here the width is not filled,

30
00:01:46,797 --> 00:01:51,073
and in here, these are flat buttons
instead of buttons.

31
00:01:51,373 --> 00:01:54,356
This is a whole mess of inconsistencies.

32
00:01:54,685 --> 00:01:56,716
So, why does this happen?

33
00:02:00,908 --> 00:02:04,152
We start with a trivial task,
implementing a search bar.

34
00:02:04,262 --> 00:02:06,005
There's no clear guideline.

35
00:02:06,114 --> 00:02:09,976
So, the HIG doesn't specify
how a search bar should be.

36
00:02:10,152 --> 00:02:12,142
We have slightly different use cases,

37
00:02:12,184 --> 00:02:15,056
a web browser, a text editor, and so on.

38
00:02:15,206 --> 00:02:17,344
So we end up with an inconsistency.

39
00:02:17,507 --> 00:02:23,241
Now, first of all, how can we
avoid that from happening?

40
00:02:23,309 --> 00:02:26,378
First of all, we talk about guidelines.

41
00:02:26,386 --> 00:02:30,057
We can have some clear and strong
human interface guidelines.

42
00:02:30,328 --> 00:02:34,670
We could improve the work that was
already done on hig.kde.org.

43
00:02:34,927 --> 00:02:37,666
New stuff should be on the HIG
before it lands.

44
00:02:37,749 --> 00:02:40,338
If I'm implementing a new component,

45
00:02:40,468 --> 00:02:44,896
it should be defined in the HIG
before I actually land the component.

46
00:02:44,939 --> 00:02:46,020
This is very important.

47
00:02:46,454 --> 00:02:49,083
Then, we can have a clear design direction.

48
00:02:49,262 --> 00:02:53,375
The VDG mainly should have an idea
of what design we are moving to.

49
00:02:53,471 --> 00:02:56,398
Having mockups here is extremely important.

50
00:02:57,231 --> 00:03:02,185
For this reason, I will have many mockups
in these presentation slides.

51
00:03:03,488 --> 00:03:08,699
Also, we need to let different
app developers talk to each other.

52
00:03:08,747 --> 00:03:11,566
Coordination between applications is essential

53
00:03:11,666 --> 00:03:14,793
when we are implementing the same feature.

54
00:03:18,150 --> 00:03:21,744
What can we do with the inconsistencies
that we already have?

55
00:03:21,823 --> 00:03:24,390
First of all, we need to find them.

56
00:03:24,493 --> 00:03:27,489
I suggest to discover all KDE apps.

57
00:03:27,648 --> 00:03:33,485
What I mean with this is going to the
kde.org/applications webpage,

58
00:03:33,621 --> 00:03:36,282
going through the list of
the whole applications,

59
00:03:36,358 --> 00:03:38,690
and trying them all out.

60
00:03:38,928 --> 00:03:40,873
This takes a lot of time,

61
00:03:40,894 --> 00:03:44,167
I did this when I originally
proposed the Consistency goal.

62
00:03:44,338 --> 00:03:50,385
And it really makes you have a feeling
of what is consistent and what isn't.

63
00:03:50,635 --> 00:03:55,251
It's really an exciting experience that
I would suggest everyone would do.

64
00:03:57,384 --> 00:04:01,786
Second, I suggest you design
reusable components.

65
00:04:02,822 --> 00:04:06,914
This is something that we
have done multiple times

66
00:04:07,099 --> 00:04:10,620
implementing a simple component
that's reusable on different places.

67
00:04:10,663 --> 00:04:13,114
We'll see how we've done that.

68
00:04:13,619 --> 00:04:16,113
Finally, we can add support
for non-KDE applications,

69
00:04:16,121 --> 00:04:19,416
because most of the world
is outside of KDE.

70
00:04:19,548 --> 00:04:23,066
We can make Firefox,
GTK applications, and so on

71
00:04:23,107 --> 00:04:24,490
feel native in Plasma,

72
00:04:24,497 --> 00:04:29,668
and that will improve the
consistency feel of the user.

73
00:04:31,055 --> 00:04:34,131
How does this improve KDE anyway?

74
00:04:34,294 --> 00:04:36,636
So first of all, we have better
software usability.

75
00:04:36,698 --> 00:04:40,062
The users will recognize
a pattern in one application

76
00:04:40,068 --> 00:04:43,815
and they will be able to use
that pattern multiple times.

77
00:04:44,168 --> 00:04:46,222
Then we have branding.

78
00:04:46,476 --> 00:04:49,617
The use of consistent visual
elements will strengthen it,

79
00:04:49,679 --> 00:04:54,255
because one person
will see one application,

80
00:04:55,096 --> 00:04:56,454
and then will see another,

81
00:04:56,462 --> 00:05:00,856
and they will recognize that both
are done by the same person.

82
00:05:01,121 --> 00:05:03,213
So that will help branding.

83
00:05:03,382 --> 00:05:04,502
Then we have less code,

84
00:05:04,531 --> 00:05:09,624
because by implementing the same component
that is used multiple times,

85
00:05:09,646 --> 00:05:12,543
we don't have to implement
new code each time.

86
00:05:12,639 --> 00:05:14,738
And it's also easier to write apps,

87
00:05:14,766 --> 00:05:17,778
because we can just use
the reusable components

88
00:05:17,827 --> 00:05:19,994
that we have previously done.

89
00:05:21,721 --> 00:05:23,250
Sure, but how did it go?

90
00:05:24,556 --> 00:05:27,023
How did it work out?
Did it actually improve KDE?

91
00:05:27,077 --> 00:05:28,599
Let's start with Plasma.

92
00:05:28,721 --> 00:05:34,057
Plasma as a whole doesn't have
many issues regarding consistency.

93
00:05:34,116 --> 00:05:38,325
It's mainly a need of tweaks
to spacing, icon sizes and so on

94
00:05:38,467 --> 00:05:41,648
when those are used inconsistently
throughout applets.

95
00:05:41,892 --> 00:05:44,451
There is work ongoing
to fix those incrementally.

96
00:05:44,499 --> 00:05:47,740
We can see applets margins
were fixed in 5.18,

97
00:05:47,747 --> 00:05:51,253
heading was introduced
in 5.19, and so on.

98
00:05:51,818 --> 00:05:56,457
So we can actually see now the
incremental updates that we have done.

99
00:05:58,781 --> 00:06:01,529
First of all, the mockups.

100
00:06:01,782 --> 00:06:06,284
These are the mockups done by Manuel.
I think they are absolutely beautiful,

101
00:06:06,361 --> 00:06:09,262
and even though we are still
missing a couple of things

102
00:06:09,473 --> 00:06:13,475
I think we really got close
to implementing them.

103
00:06:13,787 --> 00:06:19,503
We have here the notification mockup
with the plasmoid heading

104
00:06:19,816 --> 00:06:23,283
that was actually implemented in 5.19.

105
00:06:23,433 --> 00:06:27,338
We can see it in Kickoff,
in the system tray,

106
00:06:27,380 --> 00:06:32,504
in the notifications,
and finally in the new widgets sidebar.

107
00:06:32,878 --> 00:06:37,250
So this was done consistently
throughout Plasma.

108
00:06:38,074 --> 00:06:41,783
If a theme was to change
the look of the headings,

109
00:06:41,844 --> 00:06:46,030
it would apply immediately to
all these places. That's beautiful.

110
00:06:47,508 --> 00:06:50,165
Then, consistency in applets.

111
00:06:50,240 --> 00:06:56,142
We have a consistent highlight item
that was not used

112
00:06:56,343 --> 00:06:59,088
in places such as the Bluetooth applet.

113
00:06:59,299 --> 00:07:04,661
Now it uses the highlight thanks to
the introduction of a new component,

114
00:07:04,676 --> 00:07:06,349
that is the expandable list.

115
00:07:06,819 --> 00:07:11,170
The expandable list is a list
where elements can be expanded.

116
00:07:12,554 --> 00:07:16,022
This was quite important because
when we decided to do this,

117
00:07:16,145 --> 00:07:21,577
we also thought, is there anything
that we can add to this expandable--

118
00:07:21,778 --> 00:07:23,922
Yes, the Comic Sans was a joke.

119
00:07:24,918 --> 00:07:28,776
Is there anything that
we can add to this list

120
00:07:28,955 --> 00:07:33,380
so that the usability of it
will be improved?

121
00:07:33,555 --> 00:07:35,697
So we actually added this button.

122
00:07:35,820 --> 00:07:39,367
That makes the user understand
that this list element

123
00:07:39,402 --> 00:07:42,973
is able to collapse and expand.

124
00:07:44,289 --> 00:07:46,883
Because we thought we need
to do this consistently,

125
00:07:46,895 --> 00:07:48,956
we also thought "how can we improve this".

126
00:07:49,073 --> 00:07:50,302
So that's awesome.

127
00:07:50,930 --> 00:07:53,508
Then we also worked on the media player view,

128
00:07:53,517 --> 00:07:56,872
which is now, in my opinion,
absolutely gorgeous.

129
00:07:57,144 --> 00:08:00,996
We made it consistent with
our Elisa application.

130
00:08:07,004 --> 00:08:11,987
What's next?
We're working on panel icon sizes.

131
00:08:12,022 --> 00:08:18,992
This was absolutely not as easy as it seemed,
it generated a lot of discussion.

132
00:08:19,455 --> 00:08:25,052
And we're still working on it.
This is the latest proposal that you're seeing

133
00:08:25,069 --> 00:08:28,079
that hopefully will be implemented soon.

134
00:08:28,276 --> 00:08:30,939
So, we're talking about Plasma.

135
00:08:31,140 --> 00:08:37,153
We can try to see what is
the situation in applications.

136
00:08:38,217 --> 00:08:41,216
First of all, the application situation
is a bit more complex.

137
00:08:41,258 --> 00:08:44,504
There are currently two visually
different types of application.

138
00:08:44,567 --> 00:08:47,205
We have QWidgets ones
and the Kirigami ones.

139
00:08:47,599 --> 00:08:49,619
Editing a QStyle is tough,

140
00:08:49,790 --> 00:08:52,542
so it's hard to find people willing
to make QWidgets apps

141
00:08:52,543 --> 00:08:54,863
more consistent with Kirigami components.

142
00:08:55,394 --> 00:08:59,277
There has been a lot of work on
creating new Kirigami applications.

143
00:08:59,360 --> 00:09:02,524
Some fit in use cases not covered
by other KDE applications,

144
00:09:02,539 --> 00:09:05,431
such as the dialer, sound recorder and so on.

145
00:09:06,045 --> 00:09:10,644
This is mainly driven by the
development for Plasma Mobile.

146
00:09:10,889 --> 00:09:13,857
And some other might replace
the QWidget applications.

147
00:09:13,940 --> 00:09:16,428
We have Koko, which is an image reviewer,

148
00:09:16,490 --> 00:09:19,084
KAlgebra has a Kirigami version, and so on.

149
00:09:19,412 --> 00:09:23,898
There has also been a long VDG
discussion, it lasted a year,

150
00:09:23,994 --> 00:09:26,894
to decide how tabs and view
navigation should look like

151
00:09:26,950 --> 00:09:28,860
in main views and sidebar views.

152
00:09:29,278 --> 00:09:31,429
We did eventually reach a consensus,

153
00:09:31,512 --> 00:09:35,144
and we are now working
towards a clear direction,

154
00:09:35,151 --> 00:09:37,118
which is defined by this mock-up.

155
00:09:37,516 --> 00:09:39,201
So what do we have here?

156
00:09:39,433 --> 00:09:42,416
First of all, we have a new tab look.

157
00:09:42,736 --> 00:09:47,484
And then we have these.
These are technically tabs as well.

158
00:09:47,566 --> 00:09:51,184
But we decided to have a different look
using the highlighted style,

159
00:09:51,388 --> 00:09:57,769
because on this one, the user can add them,
can move them, can close them.

160
00:09:57,845 --> 00:10:01,914
But this one are defined by
the developer of the application.

161
00:10:01,942 --> 00:10:03,964
The user cannot drag them around.

162
00:10:04,161 --> 00:10:07,264
So they have to have a different style,

163
00:10:07,334 --> 00:10:10,496
so the user can understand
that they are different.

164
00:10:10,813 --> 00:10:15,271
This style was actually
implemented in Kirigami,

165
00:10:15,313 --> 00:10:17,268
if I get to the next slide...

166
00:10:17,982 --> 00:10:19,621
As you can see here.

167
00:10:19,670 --> 00:10:23,102
We have it at the bottom
when it's in phone mode,

168
00:10:23,343 --> 00:10:26,834
and it's consistent throughout
KDE applications.

169
00:10:27,624 --> 00:10:30,824
These are not mockups,
these are actual applications.

170
00:10:33,633 --> 00:10:40,053
We also are working on the new tab style.
We've added a blue line at the top.

171
00:10:41,928 --> 00:10:47,086
It's also important to notice that
Kate now uses the QStyle tab.

172
00:10:47,298 --> 00:10:54,989
So as soon as the new tab style lands,
it will also be used by Kate automatically.

173
00:10:55,132 --> 00:11:00,303
Previously Kate used an
inconsistent tab style,

174
00:11:00,387 --> 00:11:02,184
so that's a big win for us.

175
00:11:02,739 --> 00:11:03,910
What else?

176
00:11:07,564 --> 00:11:11,473
We also made the scroll bars
a bit more usable everywhere.

177
00:11:11,474 --> 00:11:14,224
They now no longer collapse automatically.

178
00:11:14,225 --> 00:11:18,748
They always stay there, so you can
easily drag them with the mouse.

179
00:11:18,926 --> 00:11:22,261
They also [?]

180
00:11:22,363 --> 00:11:28,891
and most importantly this was done
throughout KDE applications,

181
00:11:28,959 --> 00:11:32,469
Kirigami ones and QWidgets ones.

182
00:11:32,797 --> 00:11:35,939
This was done consistently,
so this is great.

183
00:11:39,319 --> 00:11:43,397
We've also worked on GTK support.

184
00:11:43,472 --> 00:11:48,167
So now GTK applications will follow
shadows, resize areas,

185
00:11:48,317 --> 00:11:52,215
and they're now following the color scheme,
which is extremely important,

186
00:11:52,252 --> 00:11:56,381
and most important, and very recently,
the decoration.

187
00:11:56,585 --> 00:11:59,962
GTK applications will now
follow your decorations.

188
00:12:01,289 --> 00:12:05,026
This will make GTK applications
feel very native,

189
00:12:05,186 --> 00:12:07,700
and the user might not even
see the difference

190
00:12:07,790 --> 00:12:10,685
between Qt and GTK applications.

191
00:12:10,843 --> 00:12:14,703
So that's very consistent of us.

192
00:12:16,318 --> 00:12:23,203
Finally, we are porting KCMs, that is
system settings sections, to Kirigami.

193
00:12:23,264 --> 00:12:27,496
We've done online accounts,
windows rules, global shortcuts,

194
00:12:28,237 --> 00:12:32,912
Bluetooth has just landed,
and we are working on NetworkManager.

195
00:12:33,357 --> 00:12:37,172
The end aim is to make System Settings
pure QML/Kirigami,

196
00:12:37,306 --> 00:12:41,230
in order for it to be completely
consistent throughout it.

197
00:12:42,491 --> 00:12:46,174
If that was too much stuff
and you didn't listen,

198
00:12:46,182 --> 00:12:49,055
overall, how is the consistency goal going?

199
00:12:49,300 --> 00:12:50,943
Pretty good, thanks for asking.

200
00:12:51,243 --> 00:12:56,567
Many tasks in the original consistency
proposal are already addressed,

201
00:12:57,098 --> 00:13:00,494
such as the light highlights.

202
00:13:00,753 --> 00:13:03,259
Some other tasks are being
actively worked upon.

203
00:13:03,444 --> 00:13:06,403
Some tasks are waiting
for a developer to kick in

204
00:13:06,459 --> 00:13:09,010
especially the ones related to QWidgets.

205
00:13:09,278 --> 00:13:16,128
So if you are a developer who wants
to help, this one is a good way.

206
00:13:16,155 --> 00:13:17,984
We'll see later how.

207
00:13:18,323 --> 00:13:21,514
Finally, some tasks are stuck
as they are controversial

208
00:13:21,526 --> 00:13:25,374
from a community point of view,
so I didn't talk much about those.

209
00:13:26,463 --> 00:13:28,640
If you want to help Consistency,

210
00:13:28,665 --> 00:13:31,059
first of all, thank you,
that's very nice of you.

211
00:13:31,263 --> 00:13:34,297
You can give a look to the tasks that we have

212
00:13:34,332 --> 00:13:37,735
on the consistency workboard
on Phabricator at this link,

213
00:13:37,770 --> 00:13:42,523
or simply by searching for consistency
in Phabricator, you will find it.

214
00:13:42,870 --> 00:13:47,297
And most importantly you can
join the VDG chat

215
00:13:47,333 --> 00:13:51,508
on Telegram at @vdgmainroom
or on Matrix.

216
00:13:51,908 --> 00:13:54,549
or you can also ping me
personally on Telegram,

217
00:13:54,550 --> 00:13:58,847
if you don't know what to do I can help you
and introduce you to the group.

218
00:14:00,839 --> 00:14:05,283
If you don't know what to do,
here are some ideas.

219
00:14:05,494 --> 00:14:08,059
First of all, we need more HIG pages.

220
00:14:08,383 --> 00:14:12,489
You have seen the new
lateral navigation component.

221
00:14:12,530 --> 00:14:18,146
This is not yet on the HIG. This is
very important that it gets there soon.

222
00:14:18,537 --> 00:14:22,767
So we also need to implement
a sidebar component using QWidgets,

223
00:14:22,837 --> 00:14:24,553
this is also very important.

224
00:14:24,775 --> 00:14:27,683
We need to improve our
new Kirigami applications

225
00:14:27,905 --> 00:14:31,697
You can try them out, report bugs, and so on.

226
00:14:31,951 --> 00:14:34,198
We need more bug reports
for inconsistencies.

227
00:14:34,253 --> 00:14:39,118
So you should try many KDE applications
and see what's inconsistent throughout them.

228
00:14:39,518 --> 00:14:43,928
And we also need more cooperation
between applications and the working groups.

229
00:14:43,975 --> 00:14:48,016
I'm not only talking about the VDG,
but often also the promo group.

230
00:14:48,357 --> 00:14:51,427
So you can just pick one and get started.

231
00:14:51,475 --> 00:14:55,195
You can write to me personally,
I have free time usually.

232
00:14:55,549 --> 00:15:00,986
And this is pretty much it.
So, do you have any questions?

233
00:15:05,402 --> 00:15:09,919
Hi, yes, we have three questions.
The first question is:

234
00:15:10,029 --> 00:15:13,927
"How do we convince developers
to switch to standard components

235
00:15:13,928 --> 00:15:15,946
"instead of using their own ones?

236
00:15:16,230 --> 00:15:20,855
"Especially in conflicts of design
or lack of features in components"

237
00:15:21,826 --> 00:15:26,465
I think that the best way to convince
developers to switch to a standard component

238
00:15:26,492 --> 00:15:31,683
is to make that component both featureful,

239
00:15:31,776 --> 00:15:35,851
it needs to have all the features
of the previous component,

240
00:15:35,852 --> 00:15:41,710
otherwise it will both not be liked by the
developer of the application and the users.

241
00:15:42,321 --> 00:15:45,751
And another important thing
is for it to be pretty,

242
00:15:45,926 --> 00:15:51,431
because often we are deceived
by the look of something,

243
00:15:51,466 --> 00:15:55,720
and if we see that it's prettier
than our current implementation,

244
00:15:55,872 --> 00:15:58,222
we could also think that it works better.

245
00:16:01,844 --> 00:16:03,272
We have another question.

246
00:16:03,343 --> 00:16:06,148
"How do you manage to
strike a good balance

247
00:16:06,224 --> 00:16:10,469
"between refreshing the visual identity
of Plasma over the years

248
00:16:10,470 --> 00:16:14,807
"while maintaining visual consistency
with all the other KDE apps?"

249
00:16:16,083 --> 00:16:20,007
This is done because, in theory,

250
00:16:20,043 --> 00:16:25,824
Plasma applications don't
implement their own styles,

251
00:16:25,846 --> 00:16:27,875
but they follow a general style.

252
00:16:27,993 --> 00:16:29,680
The QStyle, as an example.

253
00:16:30,047 --> 00:16:33,389
So by changing the QStyle,
such as the tabs,

254
00:16:33,581 --> 00:16:37,909
that change will take effect
on all applications at once,

255
00:16:38,020 --> 00:16:39,909
such as Kate as we've seen.

256
00:16:40,153 --> 00:16:43,525
What happens sometimes
is that single applications

257
00:16:43,547 --> 00:16:49,000
do not follow this general style,
as Kate was doing before the patches.

258
00:16:49,093 --> 00:16:52,633
So what we need to do is
both to make the QStyle better,

259
00:16:52,683 --> 00:16:55,250
and make the application follow the QStyle.

260
00:16:55,498 --> 00:16:57,679
This is regarding QWidgets.

261
00:16:59,909 --> 00:17:01,573
Okay. The other question:

262
00:17:02,170 --> 00:17:05,706
"Won't porting KDE applications
to QML or Kirigami

263
00:17:05,770 --> 00:17:10,406
hurt visual consistency wrt QWidgets apps?"

264
00:17:10,874 --> 00:17:15,217
Yes, but the problem is
we have to do it anyway,

265
00:17:15,242 --> 00:17:19,938
because we currently have
QML/Kirigami applications.

266
00:17:20,073 --> 00:17:23,568
We cannot just say
"okay let's stop developing QML/Kirigami"

267
00:17:23,582 --> 00:17:29,877
because the QML/Kirigami applications
are strongly needed for Plasma Mobile.

268
00:17:30,126 --> 00:17:34,250
And the fact is Kirigami was
meant to be convergent,

269
00:17:34,321 --> 00:17:39,450
So applications developed for Plasma Mobile
are to be also used on the desktop,

270
00:17:39,505 --> 00:17:42,660
which means we are supposed
in the future to use

271
00:17:42,813 --> 00:17:46,440
a convergent application done
with Kirigami on the desktop.

272
00:17:46,832 --> 00:17:51,557
Given this, we will surely
have some inconsistency

273
00:17:51,585 --> 00:17:54,094
between QML/Kirigami and QWidgets.

274
00:17:56,122 --> 00:17:58,163
We'll have to address it.

275
00:17:58,219 --> 00:18:03,515
and the best way is not to stop
porting applications to QML/Kirigami,

276
00:18:03,741 --> 00:18:09,797
but rather to try to choose
one framework that we need to use

277
00:18:09,853 --> 00:18:14,315
and currently that framework is
much closer to QML/Kirigami.

278
00:18:14,683 --> 00:18:17,959
QML is not ready to support
all applications of course,

279
00:18:17,981 --> 00:18:22,043
but we hope that in the future it will be.

280
00:18:22,187 --> 00:18:24,259
Maybe with the strict QML

281
00:18:24,266 --> 00:18:29,369
that should be much less
RAM-intensive and faster in Qt 6.

282
00:18:33,284 --> 00:18:35,509
Okay, so our last question is:

283
00:18:35,609 --> 00:18:38,412
"Which framework will be
favored in the future,

284
00:18:38,530 --> 00:18:40,496
"Kirigami or QWidgets?"

285
00:18:41,865 --> 00:18:45,470
Kirigami cannot cover all use cases.

286
00:18:45,568 --> 00:18:49,635
It's mostly a framework
for convergent apps,

287
00:18:49,957 --> 00:18:55,034
so apps that should also be used
on a tablet or on a phone.

288
00:18:55,758 --> 00:18:59,737
As an example, I do not think
that KDevelop or Krita

289
00:18:59,745 --> 00:19:01,700
will ever be ported to Kirigami,

290
00:19:01,716 --> 00:19:05,159
because that is not what
Kirigami is made for.

291
00:19:06,040 --> 00:19:10,488
However, it could happen
that a new framework

292
00:19:10,489 --> 00:19:15,629
specifically for QML desktop
application could appear.

293
00:19:15,803 --> 00:19:18,506
So there's still a bit of
uncertainty there.

