﻿1
00:06:54,703 --> 00:06:56,346
Thank you. Thank you so much.

2
00:07:00,005 --> 00:07:01,293
Thank you for having me.

3
00:07:05,741 --> 00:07:07,561
I'd like to welcome you

4
00:07:08,256 --> 00:07:10,082
to Bugs As A Service.

5
00:07:10,371 --> 00:07:12,716
This will be our company for this morning.

6
00:07:13,059 --> 00:07:16,626
Bugs As A Service makes a bug tracker A++,

7
00:07:16,953 --> 00:07:18,363
it's a fancy company,

8
00:07:19,409 --> 00:07:21,843
but it's a very small company,
it's a small team.

9
00:07:24,014 --> 00:07:26,782
Let me also introduce
our problem.

10
00:07:27,388 --> 00:07:30,254
Our problem is here represented
as a purple ball.

11
00:07:30,777 --> 00:07:33,145
We don't know much about this problem.

12
00:07:33,596 --> 00:07:36,689
We aren't really sure that it's a serious problem,

13
00:07:36,690 --> 00:07:39,447
we're not exactly sure if it is a problem at all.

14
00:07:39,632 --> 00:07:41,107
And if it is a problem,

15
00:07:41,140 --> 00:07:43,878
we're not exactly sure if we
have to fix it right now,

16
00:07:43,893 --> 00:07:47,184
or if it's easy to fix, or even if it
will take a month time to fix.

17
00:07:48,054 --> 00:07:52,094
So it's a very, very unclear
problem that we have.

18
00:07:53,903 --> 00:07:56,780
Now let's say that we could place that problem

19
00:07:56,915 --> 00:07:58,242
in some sort of system.

20
00:07:58,290 --> 00:08:00,554
So let's say that we could say
it was a red problem,

21
00:08:00,569 --> 00:08:03,033
or a yellow problem, or a blue problem,

22
00:08:03,082 --> 00:08:04,527
or a green problem.

23
00:08:04,732 --> 00:08:07,456
Maybe if we could classify
the problem somehow,

24
00:08:07,956 --> 00:08:09,229
that would tell us something.

25
00:08:11,808 --> 00:08:14,465
Or maybe how we solve problems

26
00:08:14,482 --> 00:08:16,937
tells us more about the problem itself.

27
00:08:17,002 --> 00:08:20,292
Like maybe we can distinguish
different problems from each other

28
00:08:20,314 --> 00:08:22,531
by thinking about how we solve the problems.

29
00:08:23,796 --> 00:08:27,833
So, down here let's imagine
that we have crises.

30
00:08:28,240 --> 00:08:30,280
Now this could be like an everyday crisis,

31
00:08:30,302 --> 00:08:34,352
like a house full of 3-year-olds,
for example.

32
00:08:34,783 --> 00:08:38,517
These are unfolding crises.
Stuff will happen.

33
00:08:38,562 --> 00:08:41,315
You can't just sit down and
pretend it's not there.

34
00:08:41,372 --> 00:08:42,378
Things will happen.

35
00:08:42,750 --> 00:08:45,313
If your house is burning down,
you can pretend it's not,

36
00:08:45,338 --> 00:08:46,811
but it will burn to the ground.

37
00:08:47,253 --> 00:08:50,810
So these are things that, if you don't do anything about it,

38
00:08:50,970 --> 00:08:55,405
it will resolve itself, and probably not in a way that you like.

39
00:08:56,802 --> 00:09:07,100
Now up here we have other types of problems.
Now these are problems that you don't necessarily know what the problem is
or how to solve it.
So you kinda have to experiment-

40
00:09:07,259 --> 00:09:13,431
Try out different things, debug, try to learn.
So there's a lot of unknowns in this scenario.

41
00:09:13,586 --> 00:09:18,522
A lot of detective work
to try to figure things out.

42
00:09:19,172 --> 00:09:26,457
Whereas up here you have really nice problems, delineated problems.
You know what the problem is, you know how to solve the problem,

43
00:09:26,596 --> 00:09:28,544
you're not really worried about that.

44
00:09:28,688 --> 00:09:30,705
You know that the problem can be solved.

45
00:09:30,989 --> 00:09:35,066
Now you're thinking more, "what is the
best way to solve these kinds of problem?"

46
00:09:35,246 --> 00:09:37,831
There are several ways,
but what is the best way for us?

47
00:09:38,132 --> 00:09:45,070
Maybe you have a tradition of engineering,

48
00:09:45,089 --> 00:09:48,138
and best practices you want
to apply to the problem.

49
00:09:49,553 --> 00:09:52,255
Down here we have great,
wonderful problems.

50
00:09:52,280 --> 00:09:55,141
These are problems that are solved
with automation and routine.

51
00:09:55,305 --> 00:09:57,655
These are problems we know
exactly how to do.

52
00:09:57,785 --> 00:09:59,255
we just have to do it.

53
00:09:59,401 --> 00:10:01,052
You know, sometimes we might realize

54
00:10:01,071 --> 00:10:03,332
that there's some
bug in our automation,

55
00:10:03,353 --> 00:10:06,711
or maybe some steps
missing from our checklist,

56
00:10:07,005 --> 00:10:10,253
but this is stuff that we can fix
so that next time we do it better.

57
00:10:10,396 --> 00:10:12,401
But we know how to solve these problems.

58
00:10:14,176 --> 00:10:15,921
Now you have certain people,

59
00:10:16,811 --> 00:10:18,488
that tend to work in parts of this.

60
00:10:19,072 --> 00:10:22,605
And so this system, by the way,

61
00:10:22,648 --> 00:10:25,123
is often called the Cynefin framework.

62
00:10:25,159 --> 00:10:27,493
It's made by a guy named Dave Snowden.

63
00:10:27,728 --> 00:10:30,804
And he talks about it as a
sense-making framework,

64
00:10:30,812 --> 00:10:33,166
and he has like names
for these different things.

65
00:10:33,576 --> 00:10:36,234
So down here's what he calls
the chaotic domain,

66
00:10:36,768 --> 00:10:40,513
And this is the complex domain,
and here is the complicated domain,

67
00:10:40,516 --> 00:10:42,236
and down here is the clear domain.

68
00:10:42,350 --> 00:10:44,439
So they have names.

69
00:10:45,301 --> 00:10:48,444
But I want to talk more about
the people who work there.

70
00:10:50,789 --> 00:10:54,336
You have certain people who tend
to work mostly in one domain.

71
00:10:54,724 --> 00:10:58,242
So here we have Ellis.
He works for Bugs as a Service.

72
00:10:58,839 --> 00:11:02,736
Ellis started off his career
as a graphical designer,

73
00:11:02,892 --> 00:11:06,644
now his official title
is probably UX designer.

74
00:11:06,787 --> 00:11:09,860
But he does all things design and creative

75
00:11:09,901 --> 00:11:13,092
at this very small team
at Bugs as a Service. 

76
00:11:13,650 --> 00:11:15,976
He is a very creative person,
probably would--

77
00:11:16,216 --> 00:11:18,042
he wants to be an artist.

78
00:11:18,162 --> 00:11:21,691
And he likes to play music,
so he actually has a SoundCloud

79
00:11:21,707 --> 00:11:24,827
just in case, you know, a tweet
goes viral at some point.

80
00:11:26,008 --> 00:11:31,458
And he likes to try to figure out
what is the best thing for the users,

81
00:11:32,876 --> 00:11:36,839
and does a lot of off-the-wall
thinking, and just...

82
00:11:37,097 --> 00:11:39,476
throwing ideas out there.

83
00:11:40,798 --> 00:11:44,447
Now here is Alec.
Alec does not throw ideas out there.

84
00:11:44,601 --> 00:11:45,965
That's not what he does.

85
00:11:46,464 --> 00:11:48,241
He will throw solutions out there.

86
00:11:48,269 --> 00:11:51,316
If you give him a problem,
he will try to give you a solution.

87
00:11:51,861 --> 00:11:54,003
Alec is also a little bit tired of his job.

88
00:11:54,841 --> 00:11:58,071
He's more interested in the stuff
that he does at home.

89
00:11:58,323 --> 00:12:01,758
And at home he likes to play video games,

90
00:12:01,773 --> 00:12:04,883
and he also likes to do home automation,

91
00:12:04,908 --> 00:12:09,528
and all sorts of real tech projects,
and open source stuff that he has going on.

92
00:12:09,692 --> 00:12:11,382
That's really what keeps him going,

93
00:12:11,438 --> 00:12:13,647
because he kind of thinks
his job is a little bit boring.

94
00:12:16,698 --> 00:12:18,137
Down here we have Bailey.

95
00:12:18,267 --> 00:12:19,965
Bailey loves her job.

96
00:12:20,299 --> 00:12:25,227
Bailey loves her job because it basically
means that she can make order out of chaos.

97
00:12:25,607 --> 00:12:29,662
So Bailey is an SRE,
or site reliability engineer.

98
00:12:29,819 --> 00:12:32,839
She used to be just called a sysadmin.

99
00:12:33,058 --> 00:12:37,760
The basic role she fills is just
to keep the world from...

100
00:12:39,486 --> 00:12:41,512
...descending into chaos.

101
00:12:42,119 --> 00:12:44,447
So everything has to have a system.
There should be order.

102
00:12:44,482 --> 00:12:48,901
She's not a big fan of Ellis's
off-the-wall ideas either.

103
00:12:49,173 --> 00:12:52,913
She thinks that we should have
a plan, and we should follow it,

104
00:12:52,928 --> 00:12:56,139
and noone should be all too
creative about the world.

105
00:12:57,931 --> 00:12:59,631
Down here we have Parker.

106
00:12:59,740 --> 00:13:01,964
Parker is an incident responder.

107
00:13:02,379 --> 00:13:06,074
And this means that Parker has a
very strange kind of personality.

108
00:13:06,449 --> 00:13:10,087
So, when the world is on fire,
Parker is smiling.

109
00:13:10,456 --> 00:13:14,258
When everyone feels like "oh my gosh",
and everyone is super stressed out,

110
00:13:14,314 --> 00:13:17,256
Parker is like "finally,
my moment is here".

111
00:13:17,547 --> 00:13:21,287
So Parker will smile in the
most inoportune moments,

112
00:13:21,365 --> 00:13:25,408
when things really are
not to be smiled about.

113
00:13:26,427 --> 00:13:29,803
But you have also people who don't really know where they belong.

114
00:13:30,108 --> 00:13:32,458
Like here, you have Finley.

115
00:13:32,829 --> 00:13:33,984
Finley was a dev.

116
00:13:34,652 --> 00:13:39,771
But then Finley got a promotion.
Finley's not exactly sure that being a tech lead was a promotion. 

117
00:13:39,991 --> 00:13:44,221
Because now Finley feels that most of her time is spent in meetings, and

118
00:13:44,491 --> 00:13:46,968
she actually really liked to code.

119
00:13:47,552 --> 00:13:52,275
But being in this complex domain, in this creative domain,

120
00:13:53,052 --> 00:14:00,331
is necessary for Finley, because she has to balance the role of trying to figure out what is the right system to build

121
00:14:00,569 --> 00:14:03,133
versus how to build the right system.

122
00:14:03,490 --> 00:14:05,359
When she has her dev side.

123
00:14:05,572 --> 00:14:10,389
And so balancing "what should we make" with "how should it be made"

124
00:14:10,757 --> 00:14:13,147
is a part of her role.

125
00:14:14,285 --> 00:14:17,467
We also have Oakley.
Oakley is a senior dev.

126
00:14:17,700 --> 00:14:21,118
And one of the things that Oakley has been working hard on lately

127
00:14:21,380 --> 00:14:23,230
is automation.

128
00:14:23,662 --> 00:14:30,358
So working on the CI/CD pipeline.
Making sure that when we push things to production they work properly.

129
00:14:30,540 --> 00:14:37,766
Also making sure that there's an infrastructure that can be easily put up and put down

130
00:14:37,971 --> 00:14:39,717
and all of those things.

131
00:14:39,777 --> 00:14:45,695
So that's more in the Clear domain where Oakley has been working with the automation.

132
00:14:48,530 --> 00:14:52,913
In the Clear domain you also find Kian, who does QA.

133
00:14:53,260 --> 00:14:59,271
QA means that Kian is basically everywhere all the time.
Kian is non-binary and uses they/them pronouns.

134
00:14:59,523 --> 00:15:03,604
And Kian loves to figure things out.

135
00:15:03,769 --> 00:15:09,603
And actually quite enjoys when there is a disaster in some way, together with Parker, just because

136
00:15:09,926 --> 00:15:17,046
finally, I guess, things really went as badly as Kian always worries about. It's a strange thing.

137
00:15:17,137 --> 00:15:22,086
Kian worries constantly. When things actually go bad, Kian kind of likes it.

138
00:15:23,790 --> 00:15:25,998
Also, Kian likes to investigate things,

139
00:15:26,038 --> 00:15:28,616
and maybe that's why Kian
likes it when things are bad,

140
00:15:28,645 --> 00:15:32,468
because that means that Kian can
run around and figure things out,

141
00:15:32,536 --> 00:15:34,233
what actually went wrong.

142
00:15:35,239 --> 00:15:39,153
And a little bit, you know,
making order out of chaos there.

143
00:15:42,318 --> 00:15:43,899
So let's take a look at our team.

144
00:15:44,149 --> 00:15:47,625
We have Parker, our incident
responder. Kian, our QA.

145
00:15:48,040 --> 00:15:50,883
Bailey, our site reliability engineer.

146
00:15:51,380 --> 00:15:54,547
Oakley, our senior dev,
Alec, our dev,

147
00:15:54,755 --> 00:15:58,174
Finley our tech lead,
and Ellis, our designer.

148
00:15:59,976 --> 00:16:05,509
And together, they are the parts
of Bugs As A Service, our company.

149
00:16:08,188 --> 00:16:10,197
Oh yeah, sorry...

150
00:16:10,579 --> 00:16:12,348
I almost forgot...

151
00:16:13,158 --> 00:16:15,174
This here, this is Jeff.

152
00:16:16,223 --> 00:16:21,048
Jeff is our sales guy.
He talks on the phone. A lot.

153
00:16:21,552 --> 00:16:23,602
And smiles. A lot.

154
00:16:24,342 --> 00:16:27,688
Speaks really loudly, a lot.

155
00:16:29,306 --> 00:16:33,281
Oh yeah, and here...
This is Brad.

156
00:16:34,473 --> 00:16:38,023
Jeff talks a lot with Brad on the phone,
because Brad is the customer.

157
00:16:39,399 --> 00:16:40,960
Okay, so...

158
00:16:41,273 --> 00:16:44,389
It's Saturday.
And today...

159
00:16:45,297 --> 00:16:48,891
all of these people are going
to have a really bad day.

160
00:16:51,466 --> 00:16:53,298
Welcome to my talk:

161
00:16:54,000 --> 00:16:55,718
"I can't work like this"

162
00:16:56,303 --> 00:16:59,352
And also welcome to KDE Akademy 2021.

163
00:17:01,618 --> 00:17:04,254
My name is Patricia Aas,
I'm a trainer and consultant.

164
00:17:04,431 --> 00:17:09,840
I'm a C++ programmer, I have been
programming now in C++ for about 15 years.

165
00:17:10,883 --> 00:17:14,046
I specialize in application security.

166
00:17:14,494 --> 00:17:17,629
And I work for a company that
I co-founded called TurtleSec.

167
00:17:18,324 --> 00:17:22,366
My first job was on the
original Opera browser,

168
00:17:22,965 --> 00:17:28,294
where actually we were working in
the floor above Trolltech at the time.

169
00:17:28,763 --> 00:17:33,784
Then I went over to do some Java
consultancy for a little while

170
00:17:33,794 --> 00:17:37,600
and then went to Cisco where I made
embedded telepresence systems,

171
00:17:37,676 --> 00:17:39,429
also using Qt there.

172
00:17:39,996 --> 00:17:42,675
And then I went back to making
a browser called Vivaldi,

173
00:17:42,651 --> 00:17:46,434
and when I have time, I work on
a pet project of mine,


174
00:17:46,497 --> 00:17:50,530
which is making a browser as well,
so I guess I have a thing about browsers.

175
00:17:52,312 --> 00:17:55,767
I have a master's degree in computer science
and my pronouns are she/her.

176
00:17:57,820 --> 00:17:59,631
So back to our heroes.

177
00:18:02,434 --> 00:18:05,086


178
00:18:06,047 --> 00:18:08,842


179
00:18:09,660 --> 00:18:15,077


180
00:18:15,351 --> 00:18:18,452


181
00:18:20,777 --> 00:18:23,829


182
00:18:24,313 --> 00:18:30,223


183
00:18:30,913 --> 00:18:37,672


184
00:18:39,101 --> 00:18:42,802


185
00:18:43,359 --> 00:18:45,256


186
00:18:45,542 --> 00:18:49,121


187
00:18:51,831 --> 00:18:53,985


188
00:18:54,279 --> 00:18:58,230


189
00:18:58,892 --> 00:19:00,405


190
00:19:00,554 --> 00:19:03,737


191
00:19:06,350 --> 00:19:07,779


192
00:19:08,194 --> 00:19:12,004


193
00:19:13,778 --> 00:19:15,092


194
00:19:15,955 --> 00:19:19,738


195
00:19:21,871 --> 00:19:25,624


196
00:19:26,974 --> 00:19:29,448


197
00:19:30,273 --> 00:19:33,494


198
00:19:33,640 --> 00:19:37,655


199
00:19:39,748 --> 00:19:41,300


200
00:19:42,053 --> 00:19:47,565


201
00:19:48,738 --> 00:19:51,124


202
00:19:51,214 --> 00:19:56,511


203
00:19:56,953 --> 00:19:59,415


204
00:20:00,192 --> 00:20:07,097


205
00:20:08,997 --> 00:20:10,640


206
00:20:11,914 --> 00:20:14,576


207
00:20:14,748 --> 00:20:18,293


208
00:20:19,389 --> 00:20:27,841


209
00:20:30,461 --> 00:20:32,480


210
00:20:32,651 --> 00:20:37,843


211
00:20:38,026 --> 00:20:40,384


212
00:20:41,974 --> 00:20:43,370


213
00:20:43,827 --> 00:20:46,000


214
00:20:47,711 --> 00:20:52,222


215
00:20:52,897 --> 00:20:55,942


216
00:20:57,220 --> 00:21:00,432


217
00:21:01,072 --> 00:21:03,509


218
00:21:04,828 --> 00:21:09,330


219
00:21:10,731 --> 00:21:14,100


220
00:21:14,332 --> 00:21:20,379


221
00:21:21,329 --> 00:21:28,270


222
00:21:31,782 --> 00:21:34,112


223
00:21:34,798 --> 00:21:38,656


224
00:21:39,588 --> 00:21:43,492


225
00:21:44,839 --> 00:21:48,908


226
00:21:50,087 --> 00:21:56,686


227
00:21:56,822 --> 00:21:58,584


228
00:21:58,895 --> 00:22:02,301


229
00:22:02,759 --> 00:22:06,533


230
00:22:06,648 --> 00:22:10,840


231
00:22:10,965 --> 00:22:13,542


232
00:22:13,867 --> 00:22:16,356


233
00:22:19,765 --> 00:22:25,119


234
00:22:32,531 --> 00:22:35,088


235
00:22:35,346 --> 00:22:43,376


236
00:22:43,444 --> 00:22:49,156


237
00:22:49,876 --> 00:22:53,428


238
00:22:53,893 --> 00:22:55,313


239
00:22:57,608 --> 00:22:59,482


240
00:23:00,982 --> 00:23:06,026


241
00:23:06,749 --> 00:23:09,273


242
00:23:11,278 --> 00:23:14,897


243
00:23:16,028 --> 00:23:17,335


244
00:23:19,598 --> 00:23:27,837


245
00:23:28,116 --> 00:23:29,738


246
00:23:29,964 --> 00:23:33,741


247
00:23:34,054 --> 00:23:41,045


248
00:23:43,288 --> 00:23:46,768


249
00:23:46,910 --> 00:23:49,776


250
00:23:53,737 --> 00:23:55,079


251
00:23:55,678 --> 00:24:01,681


252
00:24:06,997 --> 00:24:10,289


253
00:24:10,340 --> 00:24:16,667


254
00:24:19,083 --> 00:24:26,332


255
00:24:26,388 --> 00:24:32,817


256
00:24:36,429 --> 00:24:38,988


257
00:24:39,806 --> 00:24:42,069


258
00:24:42,406 --> 00:24:45,549


259
00:24:46,523 --> 00:24:49,088


260
00:24:49,477 --> 00:24:51,918


261
00:24:52,970 --> 00:24:59,412


262
00:24:59,881 --> 00:25:03,088


263
00:25:03,904 --> 00:25:09,007


264
00:25:09,044 --> 00:25:12,827


265
00:25:16,150 --> 00:25:20,756


266
00:25:20,824 --> 00:25:24,901


267
00:25:25,408 --> 00:25:29,764


268
00:25:30,139 --> 00:25:34,127


269
00:25:34,276 --> 00:25:39,523


270
00:25:41,868 --> 00:25:43,988


271
00:25:44,206 --> 00:25:51,602


272
00:25:52,883 --> 00:25:59,182


273
00:25:59,245 --> 00:26:06,787


274
00:26:09,481 --> 00:26:10,870


275
00:26:12,284 --> 00:26:14,458


276
00:26:14,920 --> 00:26:22,667


277
00:26:23,824 --> 00:26:26,714


278
00:26:26,938 --> 00:26:30,995


279
00:26:32,003 --> 00:26:36,311


280
00:26:37,506 --> 00:26:42,271


281
00:26:43,107 --> 00:26:46,761


282
00:26:49,754 --> 00:27:04,101


283
00:27:05,838 --> 00:27:08,070


284
00:27:08,470 --> 00:27:10,447


