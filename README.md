# Akademy subtitles

This repository will have subtitles for Akademy talk videos.
I recommend [Subtitle Composer](https://subtitlecomposer.kde.org/) to make them.

Creating subtitles is incredibly time consuming.
Contributions are very welcome, even partial ones.

If you add subtitles to half a talk and get bored, send what you have.
If you transcribe a talk without any timing information,
simply as a plain text file, send that,
someone else can synchronize them to the video as a second step.
Or you can create synchronized but "blank" subtitles
(with empty text) and someone else can type the text later.

This repository is open to all KDE developers,
so you can use a work/ branch for merge requests
instead of having to fork.
